﻿using System;

namespace TASK_1
{
    class Program
    {
        static void Main(string[] args)
        {
            int number;

            do
            {
                Console.Write("Введите число: ");
            }
            while (!int.TryParse(Console.ReadLine(), out number));

            Console.WriteLine($"Ваше число: {number}");
        }
    }
}
